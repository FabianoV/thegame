﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animations.source.implementation
{
    /// <summary>
    /// Implementation of independent animation from Animations Collection.
    /// </summary>
    public class CAnimation
    {
        // General Properties
        /// <summary> Acts as an object id. </summary>
        public string Name { get; set; }

        // XNA Object Properties
        public Vector2 Position { get; set; }
        public Vector2 Size { get; set; }
        public Rectangle Rectangle
        {
            get { return new Rectangle((int)Position.X, (int)Position.Y, (int)Size.X, (int)Size.Y); }
        }

        // Animations data Properties
        /// <summary> All frames in animation - order is important. </summary>
        public Texture2D Spritesheet { get; set; }
        public List<Frame> Frames { get; set; }
        public int FrameNum { get; set; }
        public TimeSpan Interval { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsLooped { get; set; }
        public bool IsEneded { get; set; }

        // Inner Properties
        private TimeSpan NextFrameShowTime { get; set; }
        public Rectangle CurrentFrameRectangle { get; set; }

        // Constructor
        public CAnimation()
        {
            Frames = new List<Frame>();
            SetDefaults();
        }

        // Methods
        public void SetDefaults()
        {
            FrameNum = 0; 
            Interval = TimeSpan.FromMilliseconds(100);
            IsEnabled = true;
            IsLooped = true;
            IsEneded = false;
        }

        public void Update(GameTime gameTime)
        {
            if (gameTime.TotalGameTime > NextFrameShowTime && IsEnabled)
            {
                NextFrameShowTime = gameTime.TotalGameTime + Interval;

                if (FrameNum == Frames.Count - 1) // Is Last Frame ??
                {
                    if (IsLooped)
                    {
                        FrameNum = 0;
                    }
                    else
                    {
                        IsEneded = true;
                    }
                }
                else
                {
                    FrameNum++;
                }
            }

            CurrentFrameRectangle = Frames[FrameNum].Rectangle;
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(Spritesheet, Rectangle, CurrentFrameRectangle, Color.White);
            spriteBatch.End();
        }
    }
}
