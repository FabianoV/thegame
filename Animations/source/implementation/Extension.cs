﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animations
{
    //[DebuggerStepThrough]
    public static class Extension
    {
        //[DebuggerStepThrough]
        public static void Draw(this SpriteBatch spriteBatch, IAnimationsCollection animations, Rectangle destination, Color color)
        {
            spriteBatch.Draw(animations.Spritesheet, destination, animations.Current.Rectangle, color);
        }

        [DebuggerStepThrough]
        public static void Draw(this SpriteBatch spriteBatch, IAnimationsCollection animations, Color color)
        {
            spriteBatch.Draw(animations.Spritesheet, animations.Current.Rectangle, animations.Current.Rectangle, color);
        }

        [DebuggerStepThrough]
        public static void Draw(this SpriteBatch spriteBatch, IAnimation animation, Rectangle destination, Color color)
        {
            spriteBatch.Draw(animation.Parent.Spritesheet, destination, animation.Parent.Current.Rectangle, color);
        }

        [DebuggerStepThrough]
        public static void Draw(this SpriteBatch spriteBatch, IAnimation animation, Color color)
        {
            spriteBatch.Draw(animation.Parent.Spritesheet, animation.Parent.Current.Rectangle, animation.Parent.Current.Rectangle, color);
        }
    }
}
