﻿using System;
using System.Collections.Generic;
using System.Text;
using Animations.source.implementation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Animations
{
    public class AnimationFactory : IAnimationFactory, IDisposable
    {
        //Constructor
        /// <summary>
        /// Create animations collections from special SpriteSheet file, 
        /// where one animation is described as one row of frames with the same size, and other animations are in another rows.
        /// </summary>
        public IAnimationsCollection CreateAnimationsCollection(Texture2D texture)
        {
            IAnimationsCollection collection = new AnimationsCollection(texture);
            return collection;
        }

        public IAnimationsCollection CreateAnimationsCollection(Game game, string textureName)
        {
            Texture2D texture = game.Content.Load<Texture2D>(textureName);
            return CreateAnimationsCollection(texture);
        }

        /// <summary> Create single animation. Custom approach. </summary>
        public CAnimation CreateAnimation(Texture2D spritesheet, List<Frame> frames)
        {
            return new CAnimation();
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }

        void IDisposable.Dispose()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
