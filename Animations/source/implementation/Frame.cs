﻿using Animations.source.interfaces;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animations
{
    public class Frame : ITexture
    {
        public string Name { get; set; }
        public int Number { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 Size { get; set; }
        public Rectangle Rectangle { get { return new Rectangle(Position.ToPoint(), Size.ToPoint()); } }
    }
}
