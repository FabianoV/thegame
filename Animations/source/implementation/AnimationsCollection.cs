﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Animations
{
    public class AnimationsCollection : IAnimationsCollection, IDisposable
    {
        //Static
        private static int RowCounter = 0;

        //Properties
        public Texture2D Spritesheet { get; set; }
        public Color[] Data { get; set; }
        /// <summary>
        /// This field describes that Animations library checks if spritesheet size is multiplicity of frames size.
        /// </summary>
        public bool ExactSpritesheetSize { get; set; }    

        //Properties
        public IList<IAnimation> Animations { get; set; }
        public IAnimation Current { get; set; }

        public IAnimation this[string name] { get { return GetAnimation(name); } }
        public IAnimation this[int index] { get { return Animations[index]; } }

        //Constructor
        public AnimationsCollection(Texture2D texture)
        {
            Spritesheet = texture;
            Data = new Color[Spritesheet.Width * Spritesheet.Height];
            Spritesheet.GetData(Data);
            Animations = new List<IAnimation>();
            ExactSpritesheetSize = false;
        }

        //Methods
        /// <summary>
        /// Creates animation from special one animation file.
        /// File requirements:
        /// 1) Whole spritesheet area file should be filled by frames.
        /// 2) Spritesheet size have to be as integer value multipled number of frame size (width and height).
        /// 3) All frames have to have the same size
        /// 4) In each row of a document have be one animation
        /// </summary>
        /// <param name="frameSize">One frame size in spritesheet file</param>
        public IAnimation AddAnimation(string name, Vector2 frameSize, TimeSpan interval, bool isLooped = false)
        {
            ValidSize(frameSize);

            IAnimation animation = new Animation()
            {
                Parent = this,
                Name = name,
                Frames = new List<Frame>(),
                Position = new Vector2(RowCounter * frameSize.Y, GetPreviousAnimationsHeight()),
                Interval = interval,
                IsLooped = isLooped,
                CurrentFrameNumber = 0,
                Row = RowCounter++,
                IsEnabled = true,
                IsEneded = false,
            };

            if (Current == null)
            {
                Current = animation;
            }

            int framesCount = Spritesheet.Width / (int)frameSize.X;
            for (int i = 0; i < framesCount; i++)
            {
                Frame frame = new Frame()
                {
                    Number = i,
                    Position = new Vector2(i * frameSize.X, animation.Position.Y),
                    Size = frameSize,
                };

                Color firstFramePixel = Data[Spritesheet.Width * (int)frame.Position.Y + (int)frame.Position.X];
                if (firstFramePixel != Color.White)
                {
                    animation.Frames.Add(frame);
                }
            }

            Animations.Add(animation);
            return animation;
        }

        /// <summary> Add an animation from list of frames. </summary>
        public IAnimation AddAnimation(string name, List<Frame> frames, TimeSpan interval, bool isLooped = false)
        {
            IAnimation animation = new Animation()
            {
                Parent = this,
                Name = name,
                Frames = frames,
                Position = Vector2.Zero,
                Interval = interval,
                IsLooped = isLooped,
                CurrentFrameNumber = 0,
                Row = RowCounter++,
                IsEnabled = true,
                IsEneded = false,
            };

            if (Current == null)
            {
                Current = animation;
            }

            Animations.Add(animation);
            return animation;
        }

        private void ValidSize(Vector2 frameWidth)
        {
            if (ExactSpritesheetSize)
            {
                if (Spritesheet.Width % frameWidth.X != 0)
                {
                    throw new Exception("Invalid frame width");
                }

                if (Spritesheet.Height % frameWidth.Y != 0)
                {
                    throw new Exception("Invalid frame height");
                }
            }
        }
        private int GetPreviousAnimationsHeight()
        {
            return (int)Animations.Sum(a => a.FrameSize.Y);
        }
        public IAnimation GetAnimation(string name)
        {
            return Animations.FirstOrDefault(a => a.Name == name);
        }
        public void Update(GameTime gameTime)
        {
            Current.Update(gameTime);
        }
        public void ChangeAnimation(string name)
        {
            if (Current.Name != name)
            {
                Current = this[name];
                Current.ResetFrames();
            }
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.Spritesheet = null;
            }
        }
        #endregion
    }
}
