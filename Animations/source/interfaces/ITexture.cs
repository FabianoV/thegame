﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animations.source.interfaces
{
    public interface ITexture
    {
        string Name { get; set; }
        int Number { get; set; }
        Vector2 Position { get; set; }
        Vector2 Size { get; set; }
        Rectangle Rectangle { get; }
    }
}
