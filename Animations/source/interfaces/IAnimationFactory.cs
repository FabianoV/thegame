﻿using Animations.source.implementation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Animations
{
    public interface IAnimationFactory : IDisposable
    {
        /// <summary>
        /// Create animations collections from special SpriteSheet file, 
        /// where one animation is described as one row of frames with the same size, and other animations are in another rows.
        /// </summary>
        IAnimationsCollection CreateAnimationsCollection(Texture2D texture);
        IAnimationsCollection CreateAnimationsCollection(Game game, string textureName);

        /// <summary>
        /// Create single animation. Custom approach.
        /// </summary>
        CAnimation CreateAnimation(Texture2D spritesheet, List<Frame> frames);
    }
}
