﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Animations
{
    public interface IAnimation
    {
        IAnimationsCollection Parent { get; }
        List<Frame> Frames { get; }
        string Name { get; }
        int Row { get; }
        Vector2 Position { get; set; }
        Vector2 FrameSize { get; }
        Frame Frame { get; }
        int CurrentFrameNumber { get; set; }
        bool IsEnabled { get; set; }
        bool IsLooped { get; set; }
        bool IsEneded { get; set; }
        TimeSpan Interval { get; set; }
        Rectangle Rectangle { get; }

        void Update(GameTime gameTime);
        void ResetFrames();
    }
}
