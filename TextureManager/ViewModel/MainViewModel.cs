using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using TextureManager.Models;

namespace TextureManager.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        // Consts
        public string DividedImagesDirName { get; set; } = "Divided Images";

        // Properties
        public string ImagePath { get; set; }
        public BitmapImage Image { get; set; }

        public string JsonPath { get; set; }
        public string JsonContent { get; set; }

        public string ResultDirPath { get; set; }

        // Commands
        public RelayCommand GetImagePathCommand { get; set; }
        public RelayCommand GetJsonPathCommand { get; set; }
        public RelayCommand OpenLeshyToolCommand { get; set; }
        public RelayCommand GetResultDirPathCommand { get; set; }
        public RelayCommand ProcessCommand { get; set; }

        // Constructor
        public MainViewModel()
        {
            // Properties
            ImagePath = @"C:\Users\Fabiano\Documents\Visual Studio 2017\Projects\TheGame\TextureManager\TestImage\cave.png";
            JsonPath = @"C:\Users\Fabiano\Documents\Visual Studio 2017\Projects\TheGame\TextureManager\TestImage\sprites.json";

            // Commands
            GetImagePathCommand = new RelayCommand(GetImagePath);
            GetJsonPathCommand = new RelayCommand(GetJsonPath);
            GetResultDirPathCommand = new RelayCommand(GetResultDirPath);
            OpenLeshyToolCommand = new RelayCommand(OpenLeshyTool);
            ProcessCommand = new RelayCommand(RunProcess);

            // Initialize
            //ReadImage();
            //ReadJsonContent();
        }

        // Methods
        public void ReadImage()
        {
            FileStream fileStream = new FileStream(ImagePath, FileMode.Open, FileAccess.Read);

            Image = new BitmapImage();
            Image.BeginInit();
            Image.StreamSource = fileStream;
            Image.EndInit();
        }

        public void ReadJsonContent()
        {
            if (File.Exists(JsonPath))
            {
                JsonContent = File.ReadAllText(JsonPath);
            }
            else
            {
                throw new FileNotFoundException("JSON file not found");
            }
        }

        public void GetImagePath()
        {
            OpenFileDialog dialog = new OpenFileDialog();

            bool? success = dialog.ShowDialog();

            if (success != null && success.Value == true)
            {
                ImagePath = dialog.FileName;
                ReadImage();

                RaisePropertyChanged(nameof(ImagePath));
                RaisePropertyChanged(nameof(Image));
            }
        }

        public void GetJsonPath()
        {
            OpenFileDialog dialog = new OpenFileDialog();

            bool? success = dialog.ShowDialog();

            if (success != null && success.Value == true)
            {
                JsonPath = dialog.FileName;
                JsonContent = File.ReadAllText(JsonPath);

                RaisePropertyChanged(nameof(JsonPath));
                RaisePropertyChanged(nameof(JsonContent));
            }
        }

        public void GetResultDirPath()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.ValidateNames = false;
            dialog.CheckFileExists = false;
            dialog.CheckPathExists = true;
            // Always default to Folder Selection.
            dialog.FileName = "Folder Selection.";

            bool? success = dialog.ShowDialog();

            if (success != null && success.Value == true)
            {
                ResultDirPath = dialog.FileName;
                RaisePropertyChanged(nameof(ResultDirPath));
            }
        }

        public void OpenLeshyTool()
        {
            Process.Start("https://www.leshylabs.com/apps/sstool/");
        }

        private void RunProcess()
        {
            string destDirPath = GetDividedImagesDirPath();
            CreateDividedImagesDirIfNotExists();
            List<Sprite> data = JsonConvert.DeserializeObject<List<Sprite>>(JsonContent);

            foreach (Sprite sprite in data)
            {
                string spritePath = Path.Combine(destDirPath, sprite.Name + ".png");
                Int32Rect rect = new Int32Rect(sprite.X, sprite.Y, sprite.Width, sprite.Height);
                CroppedBitmap cropedImage = new CroppedBitmap(Image, rect);
                FileStream stream = new FileStream(spritePath, FileMode.Create);
                JpegBitmapEncoder jEncoder = new JpegBitmapEncoder();
                jEncoder.Frames.Add(BitmapFrame.Create(cropedImage));
                jEncoder.Save(stream);
                stream.Close();
            }
        }

        public void CreateDividedImagesDirIfNotExists()
        {
            string path = GetDividedImagesDirPath();
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public string GetDividedImagesDirPath()
        {
            return Path.Combine(ResultDirPath, DividedImagesDirName);
        }
    }
}