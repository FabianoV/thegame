﻿using Animations;
using Autofac;
using Comora;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TheGame.Components.Base;
using TheGame.Components.Cursor;
using TheGame.Diagnostics;
using TheGame.Services;
using TheGame.Services.Collision;
using TheGame.Services.Components;
using TheGame.Services.Objects;
using TheGame.Services.Tools;

namespace TheGame
{
    public static class AutoFacBootstrap
    {
        public static IContainer Container { get; set; }

        public static void Initialize()
        {
            ContainerBuilder builder = new ContainerBuilder();

            // Register Main Game Object
            builder.RegisterType<TheGame>().AsSelf().SingleInstance();

            // Register Services and Libraries
            builder.RegisterType<AnimationFactory>().As<IAnimationFactory>().SingleInstance();
            builder.RegisterType<BoxCollisionStoreService>().As<IBoxCollisionStoreService>().SingleInstance();
            builder.RegisterType<BoxCollisionCalculatorService>().As<IBoxCollisionCalculatorService>().SingleInstance();
            builder.RegisterType<BoxCollisionVisualizationService>().As<IBoxCollisionVisualisationService>().SingleInstance();
            builder.RegisterType<ComponentsService>().As<IComponentsService>().SingleInstance();
            builder.RegisterType<InstancesService>().As<IInstancesService>().SingleInstance();
            builder.RegisterType<CollisionDefinition>().As<ICollisionDefinition>().SingleInstance();
            builder.RegisterType<PixiJSReader>().As<IPixiJSReader>();

            // Register Components
            builder.RegisterAssemblyTypes(Assembly.GetCallingAssembly())
                .Where(t => t.IsSubclassOf(typeof(DrawableGameComponent)))
                .AsSelf()
                .As<DrawableGameComponent>()
                .SingleInstance();

            // Register Component Instances 
            builder.RegisterAssemblyTypes(Assembly.GetCallingAssembly())
                .Where(t => t.IsSubclassOf(typeof(BaseInstance)))
                .As<BaseInstance>()
                .AsImplementedInterfaces()
                .AsSelf()
                .InstancePerDependency();

            // Register Debugging tools
            builder.RegisterType<SettingsPanel>().AsSelf().SingleInstance();

            Container = builder.Build();
        }

        public static T Get<T>()
        {
            return Container.Resolve<T>();
        }

        public static object Get(Type type)
        {
            return Container.Resolve(type);
        }
    }
}
