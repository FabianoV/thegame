﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.Models.PixiJSReader
{
    public class PixiJSFrame
    {
        [JsonProperty]
        public string Name { get; set; }
        [JsonProperty]
        public PixiJSRectangle Frame { get; set; }
        [JsonProperty]
        public bool Rotated { get; set; }
        [JsonProperty]
        public bool Trimmed { get; set; }
        [JsonProperty]
        public PixiJSRectangle SpriteSourceSize { get; set; }
        [JsonProperty]
        public PixiJSSize SourceSize { get; set; }
    }
}
