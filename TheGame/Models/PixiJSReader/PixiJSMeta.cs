﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.Models.PixiJSReader
{
    public class PixiJSMeta
    {
        [JsonProperty]
        public string App { get; set; }
        [JsonProperty]
        public string Version { get; set; }
        [JsonProperty]
        public string Image { get; set; }
        [JsonProperty]
        public string Format { get; set; }
        [JsonProperty]
        public PixiJSSize Size { get; set; }
    }
}
