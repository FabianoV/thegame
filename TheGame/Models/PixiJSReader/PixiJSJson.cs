﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.Models.PixiJSReader
{
    public class PixiJSJson
    {
        [JsonProperty]
        public List<PixiJSFrame> Frames { get; set; }
        [JsonProperty]
        public PixiJSMeta Meta { get; set; }
    }
}
