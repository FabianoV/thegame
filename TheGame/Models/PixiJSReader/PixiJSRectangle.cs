﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.Models.PixiJSReader
{
    public class PixiJSRectangle
    {
        [JsonProperty]
        public int X { get; set; }
        [JsonProperty]
        public int Y { get; set; }

        [JsonProperty]
        public int W { get; set; }
        [JsonProperty]
        public int H { get; set; }
    }
}
