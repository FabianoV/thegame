﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.Models.PixiJSReader
{
    public class PixiJSSize
    {
        [JsonProperty]
        public int W { get; set; }
        [JsonProperty]
        public int H { get; set; }
    }
}
