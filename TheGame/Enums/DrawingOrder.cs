﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.Services
{
    /// <summary>
    /// Enumeration that represents drawing order (all instances) for each component.
    /// </summary>
    public enum DrawingOrder
    {
        BOTTOM_LAYER,    //Do not use this as a layer
        BackgoundTiles,
        BackgroundObjects,     //back-layer
        Particles,
        Player,
        MenuBack,
        MenuFront,      //front-layer
        Cursor,
        TOP_LAYER,
    }
}
