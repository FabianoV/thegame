﻿using Animations;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGame.Components.Base;
using TheGame.Services;
using TheGame.Services.Camera;

namespace TheGame.Components.Player
{
    public partial class PlayerComponent : BaseComponent<PlayerInstance>
    {
        // Consts
        public const string WalkDownAnimationName = "Player Walking Down";
        public const string WalkUpAnimationName = "Player Walking Up";
        public const string WalkLeftAnimationName = "Player Walking Left";
        public const string WalkRightAnimationName = "Player Walking Right";

        // Properties
        public IAnimation WalkDownAnimation { get; set; }
        public IAnimation WalkUpAnimation { get; set; }
        public IAnimation WalkLeftAnimation { get; set; }
        public IAnimation WalkRightAnimation { get; set; }

        // Constructor
        public PlayerComponent(TheGame game, IAnimationFactory animationFactory)
            : base(game, animationFactory)
        {
            Game = game;
            AnimationFactory = animationFactory;
        }

        // Methods
        public override void Initialize()
        {
            Id = new Guid("05bd4aee-7539-4ade-a050-a141ac0b0700");
            Name = "Player";
            TextureFileName = "player_walk_16x22";
            TextureSize = new Vector2(16, 22);
            DrawOrder = (int)DrawingOrder.Player;
            DrawingPositionType = PositionType.World;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            LoadAnimationTexture();
            WalkDownAnimation = Animations.AddAnimation(WalkDownAnimationName, TextureSize, TimeSpan.FromMilliseconds(200), isLooped: true);
            WalkUpAnimation = Animations.AddAnimation(WalkUpAnimationName, TextureSize, TimeSpan.FromMilliseconds(200), isLooped: true);
            WalkLeftAnimation = Animations.AddAnimation(WalkLeftAnimationName, TextureSize, TimeSpan.FromMilliseconds(200), isLooped: true);
            WalkRightAnimation = Animations.AddAnimation(WalkRightAnimationName, TextureSize, TimeSpan.FromMilliseconds(200), isLooped: true);

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            UpdateInstances(gameTime);
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            DrawInstances(gameTime);
            base.Draw(gameTime);
        }
    }
}
