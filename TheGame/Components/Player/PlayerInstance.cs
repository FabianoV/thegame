﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGame.Components.Base;
using TheGame.Services;

namespace TheGame.Components.Player
{
    public class PlayerInstance : BaseInstance
    {
        // Properties
        public Vector2 Speed { get; set; }          // pixels per second
        public Vector2 Acceleration { get; set; }   // pixel per frame
        public Vector2 MaximumSpeed { get; set; }
        public Direction CurrentDirection { get; set; }

        public Keys LeftKey { get; set; } = Keys.A;
        public Keys RightKey { get; set; } = Keys.D;
        public Keys UpKey { get; set; } = Keys.W;
        public Keys DownKey { get; set; } = Keys.S;

        // Constructor
        public PlayerInstance(TheGame game) 
            : base(game)
        {
            Scale = 2f;
            Position = new Vector2() { X = 100, Y = 100, };
            Speed = Vector2.Zero;
            MaximumSpeed = new Vector2(200);
            Acceleration = new Vector2(10);
        }

        // Methods
        public override void Update(GameTime gameTime)
        {
            var state = Keyboard.GetState();

            // Direction
            Direction newDirection = GetPlayerMovementDirection(state);
            if (IsDirectionChanged(newDirection))
            {
                CurrentDirection = newDirection;
                ChangeAnimationState(newDirection);
            }

            // Acceleration
            if (IsPlayerMoving(newDirection))
            {
                if (newDirection == Direction.None)
                {
                    Speed = Vector2.Zero;
                }

                if (newDirection == Direction.Left)
                {
                    if (CurrentDirection == Direction.Left)
                    {
                        float newSpeedX = Speed.X - Acceleration.X;
                        Speed = new Vector2(newSpeedX, Speed.Y);
                    }
                    else if (CurrentDirection == Direction.Right)
                    {
                        float newSpeedX = Speed.X + Acceleration.X * 4;
                        Speed = new Vector2(newSpeedX, Speed.Y);
                    }
                    else
                    {
                        float newSpeedX = Speed.X + Acceleration.X;
                        Speed = new Vector2(newSpeedX, Speed.Y);
                    }
                }

                if (newDirection == Direction.Right)
                {
                    if (CurrentDirection == Direction.Right)
                    {
                        float newSpeedX = Speed.X + Acceleration.X;
                        Speed = new Vector2(newSpeedX, Speed.Y);
                    }
                    else if (CurrentDirection == Direction.Right)
                    {
                        float newSpeedX = Speed.X - Acceleration.X * 4;
                        Speed = new Vector2(newSpeedX, Speed.Y);
                    }
                    else
                    {
                        float newSpeedX = Speed.X - Acceleration.X;
                        Speed = new Vector2(newSpeedX, Speed.Y);
                    }
                }

                if (newDirection == Direction.Down)
                {
                    if (CurrentDirection == Direction.Down)
                    {
                        float newSpeedY = Speed.Y + Acceleration.Y;
                        Speed = new Vector2(Speed.X, newSpeedY);
                    }
                    else if (CurrentDirection == Direction.Up)
                    {
                        float newSpeedY = Speed.Y - Acceleration.Y * 4;
                        Speed = new Vector2(Speed.X, newSpeedY);
                    }
                    else
                    {
                        float newSpeedY = Speed.Y - Acceleration.Y;
                        Speed = new Vector2(Speed.X, newSpeedY);
                    }
                }

                if (newDirection == Direction.Up)
                {
                    if (CurrentDirection == Direction.Up)
                    {
                        float newSpeedY = Speed.Y - Acceleration.Y;
                        Speed = new Vector2(Speed.X, newSpeedY);
                    }
                    else if (CurrentDirection == Direction.Down)
                    {
                        float newSpeedY = Speed.Y + Acceleration.Y * 4;
                        Speed = new Vector2(Speed.X, newSpeedY);
                    }
                    else
                    {
                        float newSpeedY = Speed.Y + Acceleration.Y;
                        Speed = new Vector2(Speed.X, newSpeedY);
                    }
                }

                if (Speed.Length() >= MaximumSpeed.Length())
                {
                    var speedNormalized = Speed;
                    speedNormalized.Normalize();
                    Speed = new Vector2(speedNormalized.X * MaximumSpeed.X, speedNormalized.Y * MaximumSpeed.Y);
                }
            }
            else
            {
                Speed = Vector2.Zero;
            }

            // Position
            Position = CalculateNewPosition(Position, gameTime.ElapsedGameTime);
            bool collisionExists = false;
            if (collisionExists)
            {
                ChangeAnimationState(Direction.None);
            }

            Game.Camera.Position = Position;

            base.Update(gameTime);
        }

        public Vector2 CalculateNewPosition(Vector2 oldPosition, TimeSpan elapsedTime)
        {
            Vector2 newPosition = new Vector2()
            {
                X = oldPosition.X + Speed.X * (float)elapsedTime.TotalSeconds,
                Y = oldPosition.Y + Speed.Y * (float)elapsedTime.TotalSeconds,
            };

            return newPosition;
        }

        public bool IsDirectionChanged(Direction newDirection)
        {
            if (newDirection != CurrentDirection)
            {
                return true;
            }

            return false;
        }

        public bool IsPlayerMoving(Direction direction)
        {
            if (direction == Direction.None && Speed.Length() == 0)
            {
                return false;
            }

            return true;
        }

        public void ChangeAnimationState(Direction direction)
        {
            switch (direction)
            {
                case Direction.Left: 
                    Animations.ChangeAnimation(PlayerComponent.WalkLeftAnimationName);
                    break;
                case Direction.Right:
                    Animations.ChangeAnimation(PlayerComponent.WalkRightAnimationName);
                    break;
                case Direction.Up:
                    Animations.ChangeAnimation(PlayerComponent.WalkUpAnimationName);
                    break;
                case Direction.Down:
                    Animations.ChangeAnimation(PlayerComponent.WalkDownAnimationName);
                    break;
                default:
                    break;
            }
        }

        public Direction GetPlayerMovementDirection(KeyboardState state)
        {
            if (state.IsKeyDown(LeftKey))
            {
                return Direction.Left;
            }

            if (state.IsKeyDown(RightKey))
            {
                return Direction.Right;
            }

            if (state.IsKeyDown(UpKey))
            {
                return Direction.Up;
            }

            if (state.IsKeyDown(DownKey))
            {
                return Direction.Down;
            }

            return Direction.None;
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
