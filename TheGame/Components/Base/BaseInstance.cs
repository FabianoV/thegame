﻿using Animations;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGame.Services;
using TheGame.Services.Collision;

namespace TheGame.Components.Base
{
    public abstract class BaseInstance : IBoxCollisionInstance
    {
        // Component Items
        public TheGame Game { get; set; }
        public IAnimationsCollection Animations { get; set; }
        public IBoxCollisionStoreService CollisionStore { get; set; }
        public IBoxCollisionVisualisationService CollisionVisualizations { get; set; }

        // Properties
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Vector2 TextureSize { get; set; }
        public Vector2 Position { get; set; } = Vector2.Zero;
        public float Scale { get; set; } = 1.0f;
        public Vector2 DrawingRectangle { get; set; }
        public Color Mask { get; set; } = Color.White;

        public bool CollisionEnabled { get; set; } = true;
        public List<Rectangle> CollisionBoxes { get; set; } = new List<Rectangle>();
        public Color CollisionBoxColor { get; set; } = Color.White;

        // Constructor
        public BaseInstance(TheGame game)
        {
            Game = game;
            CollisionStore = AutoFacBootstrap.Get<IBoxCollisionStoreService>();
            CollisionVisualizations = AutoFacBootstrap.Get<IBoxCollisionVisualisationService>();
        }

        // Methods
        public void Initialize(Vector2 textureSize)
        {
            TextureSize = textureSize;
        }

        public virtual Rectangle GetDrawingRectangle()
        {
            var rect = new Rectangle((int)Position.X, (int)Position.Y, (int)DrawingRectangle.X, (int)DrawingRectangle.Y);
            return rect;
        }

        public virtual void RecalculateDrawingRectangleByImageScale()
        {
            DrawingRectangle = new Vector2()
            {
                X = TextureSize.X * Scale,
                Y = TextureSize.Y * Scale,
            };
        }

        public virtual bool Validate()
        {
            if (Configuration.ThrowExceptionsInValidation)
            {
                if (Position == null)
                {
                    throw new ArgumentException("Position cannot be not initialized!");
                }

                if (DrawingRectangle == null || DrawingRectangle.X == 0 || DrawingRectangle.Y == 0)
                {
                    throw new ArgumentException("DrawingTextureSize cannot be not initialized or value cannot be 0!");
                }

                if (Scale <= 0)
                {
                    throw new ArgumentException("Scale must be grater than 0! Default value for normal size is 1.");
                }

                if (Mask == null)
                {
                    throw new ArgumentException("Mask must be set and cannot be null!");
                }
            }
            else
            {
                if (Position == null)
                {
                    return false;
                }

                if (DrawingRectangle == null || DrawingRectangle.X == 0 || DrawingRectangle.Y == 0)
                {
                    return false;
                }

                if (Scale <= 0)
                {
                    return false;
                }

                if (Mask == null)
                {
                    return false;
                }
            }

            return true;
        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void Draw(GameTime gameTime)
        {
            if (Animations == null)
            {
                throw new Exception("Animations here cannot be null!");
            }

            Game.SpriteBatch.Draw(Animations, GetDrawingRectangle(), Mask);
        }
    }
}
