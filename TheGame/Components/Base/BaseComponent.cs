﻿using Animations;
using Comora;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGame.Services;
using TheGame.Services.Camera;

namespace TheGame.Components.Base
{
    /// <summary>
    /// Base component that represents simple physical drawable object in game.
    /// </summary>
    public abstract class BaseComponent<InstanceType> : DrawableGameComponent, IGameComponent
        where InstanceType : BaseInstance
    {
        // Services
        public new TheGame Game { get; set; }
        public IAnimationFactory AnimationFactory { get; set; }

        // Properties
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<InstanceType> Instances { get; set; }
        public string TextureFileName { get; set; }
        public Vector2 TextureSize { get; set; }
        public IAnimationsCollection Animations { get; set; }
        public PositionType DrawingPositionType { get; set; } = PositionType.World;

        // Constructor
        public BaseComponent(TheGame game, IAnimationFactory animationFactory)
            : base(game)
        {
            Game = game;
            AnimationFactory = animationFactory;
            Instances = new List<InstanceType>();
            DrawOrder = (int)DrawingOrder.TOP_LAYER;
            DrawingPositionType = PositionType.Camera;
        }

        // Methods
        /// <summary>
        /// Allow to register specific instance that represents one drawable object of current component.
        /// </summary>
        public void RegisterInstance(InstanceType instance)
        {
            // Initialize with component data
            if (instance.Animations == null)
            {
                instance.Animations = Animations;
            }
            if (instance.TextureSize == Vector2.Zero)
            {
                instance.TextureSize = TextureSize;
            }
            if (instance.DrawingRectangle == Vector2.Zero)
            {
                instance.DrawingRectangle = TextureSize;
            }

            Instances.Add(instance);
        }

        /// <summary>
        /// Remove insatnce added before.
        /// </summary>
        /// <param name="instance"></param>
        public void RemoveInstance(InstanceType instance)
        {
            Instances.Remove(instance);
        }

        /// <summary>
        /// Validates all instances and component properties and throws errror when any property have forbidden or not initialized value.
        /// </summary>
        public bool Validate()
        {
            if (Configuration.ThrowExceptionsInValidation)
            {
                if (Id == Guid.Empty)
                {
                    throw new ArgumentException("Id have to be set in component!");
                }

                if (string.IsNullOrWhiteSpace(Name))
                {
                    throw new ArgumentException("ComponentName have to be set in component!");
                }

                if (string.IsNullOrWhiteSpace(TextureFileName))
                {
                    throw new ArgumentException("Not set up any texture file name!");
                }

                if (TextureSize == null || TextureSize.X == 0 || TextureSize.Y == 0)
                {
                    throw new ArgumentException("PredefinedTextureSize must be set and cannot have 0 value.");
                }

                if (Animations == null)
                {
                    throw new ArgumentException("Animations are not loaded!");
                }

                if (Animations.Animations.Count == 0)
                {
                    throw new ArgumentException("No any animation registered yet.");
                }
            }

            foreach (InstanceType instance in Instances)
            {
                var validationResult = instance.Validate();

                if (validationResult == false)
                {
                    return false;
                }
            }

            return true;
        }

        public void LoadAnimationTexture()
        {
            string texturePath = GetTexturePath();
            Animations = AnimationFactory.CreateAnimationsCollection(Game, texturePath);
        }

        public string GetTexturePath()
        {
            return $"Images/{Name}Component/{TextureFileName}";
        }

        public void UpdateInstances(GameTime gameTime)
        {
            foreach (InstanceType instance in Instances)
            {
                instance.Update(gameTime);
            }

            Animations.Update(gameTime);
        }

        public void DrawInstances(GameTime gameTime)
        {
            if (DrawingPositionType == PositionType.World)
            {
                Game.SpriteBatch.Begin(Game.Camera, SpriteSortMode.BackToFront);
            }
            else
            {
                Game.SpriteBatch.Begin(SpriteSortMode.BackToFront);
            }

            foreach (BaseInstance componentInstance in Instances)
            {
                componentInstance.Draw(gameTime);
            }

            Game.SpriteBatch.End();
        }
    }
}
