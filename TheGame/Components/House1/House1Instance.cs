﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGame.Components.Base;
using TheGame.Services;

namespace TheGame.Components.House1
{
    public class House1Instance : BaseInstance
    {
        // Constructor
        public House1Instance(TheGame game) 
            : base(game)
        {
            Position = Vector2.Zero;
            Scale = 1;
            CollisionBoxes = new List<Rectangle>();
            CollisionBoxColor = Color.Blue;
        }

        // Methods
        public override void Update(GameTime gameTime)
        {
            CollisionBoxes.Clear();
            Rectangle updatedCollisionBox = new Rectangle(Position.ToPoint(), DrawingRectangle.ToPoint());
            CollisionBoxes.Add(updatedCollisionBox);

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
