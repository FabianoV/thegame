﻿using Animations;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGame.Components.Base;
using TheGame.Services;
using TheGame.Services.Camera;

namespace TheGame.Components.House1
{
    public partial class House1Component : BaseComponent<House1Instance>
    {
        // Properties
        public IAnimation HouseAnimation { get; set; }

        // Constructor
        public House1Component(TheGame game, IAnimationFactory animationFactory)
            : base(game, animationFactory)
        {
            Game = game;
            AnimationFactory = animationFactory;
        }

        // Methods
        public override void Initialize()
        {
            Id = new Guid("6c1332ff-5410-445c-aae0-3973696f396e");
            Name = "House1";
            TextureFileName = "House1Front_76x81";
            TextureSize = new Vector2(76, 81);
            DrawOrder = (int)DrawingOrder.BackgroundObjects;
            DrawingPositionType = PositionType.World;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            LoadAnimationTexture();
            HouseAnimation = Animations.AddAnimation("House animation", TextureSize, TimeSpan.FromMilliseconds(200), isLooped: true);
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            UpdateInstances(gameTime);
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            DrawInstances(gameTime);
            base.Draw(gameTime);
        }
    }
}
