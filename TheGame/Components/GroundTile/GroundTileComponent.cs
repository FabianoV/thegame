﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Animations;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TheGame.Components.Base;
using TheGame.Services;
using TheGame.Services.Camera;

namespace TheGame.Components.GroundTile
{
    public class GroundTileComponent : BaseComponent<GroundTileInstance>
    {
        // Consts
        public const string LeftDownCornerTile = "Left Down Corner Tile";

        // Properties
        public Texture2D Texture { get; set; }
        public Dictionary<string, Rectangle> Frames { get; set; }

        // Constructor
        public GroundTileComponent(TheGame game, IAnimationFactory animationFactory) 
            : base(game, animationFactory)
        {
        }

        // Methods
        public override void Initialize()
        {
            Id = new Guid("6bb7723e-bef5-4ac7-8042-ad5dca691c6a");
            Name = "GroundTile";
            TextureFileName = "emptygroundtiles_16x16";
            TextureSize = new Vector2(16, 16);
            DrawOrder = (int)DrawingOrder.BackgroundObjects;
            DrawingPositionType = PositionType.World;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            string texturePath = GetTexturePath();
            Texture = Game.Content.Load<Texture2D>(texturePath);

            Frames = new Dictionary<string, Rectangle>()
            {
                { "Right Down Corner Tile", new Rectangle(0, 0, 16, 16) },
                { "Down Border", new Rectangle(16, 16, 16, 16) },
                { "Left Down Corner", new Rectangle(32, 32, 16, 16) },
            };

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {

        }

        public override void Draw(GameTime gameTime)
        {

        }
    }
}
