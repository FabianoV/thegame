﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGame.Components.Base;

namespace TheGame.Components.GroundTile
{
    public class GroundTileInstance : BaseInstance
    {
        // Properties


        // Constructor
        public GroundTileInstance(TheGame game)
            : base(game)
        {

        }

        // Methods
        public override void Update(GameTime gameTime)
        {


            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
