﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Animations;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using TheGame.Components.Base;
using TheGame.Services;

namespace TheGame.Components.Cursor
{
    public class CursorInstance : BaseInstance
    {
        // Properties
        

        // Events
        public event EventHandler LeftButtonPressed;
        public event EventHandler RightButtonPressed;
        public event EventHandler LeftButtonReleased;
        public event EventHandler RightButtonReleased;

        // Constructor
        public CursorInstance(TheGame game) 
            : base(game)
        {
            DrawingRectangle = new Vector2(25, 25);
            CollisionBoxes = new List<Rectangle>();
            CollisionBoxColor = Color.Red;
        }

        // Methods
        public override void Update(GameTime gameTime)
        {
            MouseState mouseState = Mouse.GetState();
            Position = new Vector2(mouseState.X, mouseState.Y);

            CollisionBoxes.Clear();
            Rectangle updatedCollisionBox = new Rectangle(Position.ToPoint(), DrawingRectangle.ToPoint());
            CollisionBoxes.Add(updatedCollisionBox);

            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                LeftButtonPressed?.Invoke(this, EventArgs.Empty);
            }

            if (mouseState.RightButton == ButtonState.Pressed)
            {
                RightButtonPressed?.Invoke(this, EventArgs.Empty);
            }

            if (mouseState.LeftButton == ButtonState.Released)
            {
                LeftButtonReleased?.Invoke(this, EventArgs.Empty);
            }

            if (mouseState.RightButton == ButtonState.Released)
            {
                RightButtonReleased?.Invoke(this, EventArgs.Empty);
            }

            base.Update(gameTime);
        }


    }
}
