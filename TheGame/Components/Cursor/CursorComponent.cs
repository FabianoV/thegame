﻿using Animations;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGame.Components.Base;
using TheGame.Services;
using TheGame.Services.Camera;

namespace TheGame.Components.Cursor
{
    public partial class CursorComponent : BaseComponent<CursorInstance>
    {
        // Properties
        public IAnimation CursorAnimation { get; set; }

        // Constructor
        public CursorComponent(TheGame game, IAnimationFactory animationFactory)
            : base(game, animationFactory)
        {
            Game = game;
            AnimationFactory = animationFactory;
        }

        // Methods
        public override void Initialize()
        {
            Id = new Guid("77b45aa1-2ae8-4cbc-b60a-fbf2ac031bb1");
            Name = "Cursor";
            TextureFileName = "cursor_512x512";
            TextureSize = new Vector2(512, 512);
            DrawOrder = (int)DrawingOrder.Cursor;
            DrawingPositionType = PositionType.Camera;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            LoadAnimationTexture();
            CursorAnimation = Animations.AddAnimation("cursor_animation", TextureSize, TimeSpan.Zero, false);
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            foreach (CursorInstance instance in Instances)
            {
                instance.Update(gameTime);
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            DrawInstances(gameTime);
            base.Draw(gameTime);
        }
    }
}
