﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGame.Components.Base;

namespace TheGame.Components.Mario
{
    public class MarioInstance : BaseInstance
    {
        // Properties


        // Constructor
        public MarioInstance(TheGame game) 
            : base(game)
        {
            DrawingRectangle = new Vector2(16, 32);
            Position = new Vector2(0, 0);
            CollisionBoxes = new List<Rectangle>();
            CollisionBoxColor = Color.Red;
        }

        // Methods

    }
}
