﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Animations;
using Animations.source.implementation;
using Animations.source.interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TheGame.Components.Base;
using TheGame.Models.PixiJSReader;
using TheGame.Services;
using TheGame.Services.Camera;
using TheGame.Services.Tools;

namespace TheGame.Components.Mario
{
    public class MarioComponent : BaseComponent<MarioInstance>
    {
        // Properties
        public IAnimation MarionStandAnimation { get; set; }
        public IPixiJSReader PixiJSReader { get; set; }

        public CAnimation MarioBigStandAnimation { get; set; }

        public Texture2D Spritesheet { get; set; }
        public List<Frame> Textures { get; set; }

        // Constructor
        public MarioComponent(TheGame game, IAnimationFactory animationFactory, IPixiJSReader pixiJSReader) 
            : base(game, animationFactory)
        {
            PixiJSReader = pixiJSReader;

            Name = "Mario";
            DrawOrder = (int)DrawingOrder.Player;
            DrawingPositionType = PositionType.World;
            Textures = new List<Frame>();
        }

        // Methods
        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            var marioRedBigJsonPath = "./Content/Images/Mario/Mario/Red Big/Mario Red Big.json";
            var marioRedSmallJsonPath = "./Content/Images/Mario/Mario/Red Small/Mario Red Small.json";
            // var luigiGreenBigJsonPath = "./Content/Images/Mario/Mario/Red Big/Mario Red Big.json";
            // var luigiGreenSmallPath = "./Content/Images/Mario/Mario/Red Big/Mario Red Big.json";

            var marioRedBigJson = PixiJSReader.ReadFromFile(marioRedBigJsonPath);
            var marioRedSmallJson = PixiJSReader.ReadFromFile(marioRedSmallJsonPath);
            // var luigiGreenBigJson = PixiJSReader.ReadFromFile(luigiGreenBigJsonPath);
            // var luigiGreenSmallJson = PixiJSReader.ReadFromFile(luigiGreenSmallPath);

            var marioRedBigTexture = Game.Content.Load<Texture2D>(@"Images\Mario\Mario\Red Big\Mario Red Big");
            var marioRedSmallTexture = Game.Content.Load<Texture2D>(@"Images\Mario\Mario\Red Small\Mario Red Small");
            // var luigiGreenBigTexture = Game.Content.Load<Texture2D>(@"Images\Mario\Luigi\Green Big\Luigi Green Big");
            // var luigiGreenSmallTexture = Game.Content.Load<Texture2D>(@"Images\Mario\Luigi\Green Small\Luigi Green Small");

            MarioBigStandAnimation = new CAnimation()
            {
                Name = "Mario Big Stand Animation",
                Size  = new Vector2(16, 32),
                Spritesheet = marioRedBigTexture,
                Frames = PixiJSFramesToFrames("Mario Big Stand Animation", marioRedBigJson.Frames),
            };

            Animations = AnimationFactory.CreateAnimationsCollection(Spritesheet);

            var marioStandFrames = new List<Frame>() { Textures.First(t => t.Name == "Mario Stand") };
            MarionStandAnimation = Animations.AddAnimation("Mario Stand", marioStandFrames, TimeSpan.FromMilliseconds(200), isLooped: true);

            base.LoadContent();

        }

        public override void Update(GameTime gameTime)
        {
            MarioBigStandAnimation.Update(gameTime);

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            MarioBigStandAnimation.Draw(Game.SpriteBatch, gameTime);

            // DrawInstances(gameTime);
            base.Draw(gameTime);
        }

        private List<Frame> PixiJSFramesToFrames(string animationName, List<PixiJSFrame> pixiJSFrames)
        {
            var result = new List<Frame>();
            int frameCounter = 0;

            foreach (PixiJSFrame pjsFrame in pixiJSFrames)
            {
                var frame = new Frame()
                {
                    Name = $"{animationName} {frameCounter}",
                    Number = frameCounter,
                    Position = new Vector2(pjsFrame.Frame.X, pjsFrame.Frame.Y),
                    Size = new Vector2(pjsFrame.Frame.W, pjsFrame.Frame.H),
                };
                result.Add(frame);
                frameCounter++;
            }

            return result;
        }
    }
}
