﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGame.Components.Base;

namespace TheGame.Components.Log1Component
{
    public class Log1Instance : BaseInstance
    {
        // Properites

        // Constructor
        public Log1Instance(TheGame game) 
            : base(game)
        {
            Scale = 1.0f;
            Position = new Vector2(100, 150);
        }

        // Methods
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
