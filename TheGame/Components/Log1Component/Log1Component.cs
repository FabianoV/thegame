﻿using Animations;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGame.Components.Base;
using TheGame.Services;
using TheGame.Services.Camera;

namespace TheGame.Components.Log1Component
{
    public partial class Log1Component : BaseComponent<Log1Instance>
    {
        // Properties
        public IAnimation LogAnimation { get; set; }

        //Constructor
        public Log1Component(TheGame game, IAnimationFactory animationFactory)
            : base(game, animationFactory)
        {
            Game = game;
            AnimationFactory = animationFactory;
        }

        // Methods
        public override void Initialize()
        {
            Name = "Log1";
            TextureFileName = "log_46x16";
            TextureSize = new Vector2(46, 16);
            DrawOrder = (int)DrawingOrder.BackgroundObjects;
            DrawingPositionType = PositionType.World;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            LoadAnimationTexture();
            LogAnimation = Animations.AddAnimation("Log Animation", TextureSize, TimeSpan.FromMilliseconds(200), isLooped: true);
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            DrawInstances(gameTime);
            base.Draw(gameTime);
        }
    }
}
