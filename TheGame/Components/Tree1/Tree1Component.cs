﻿using Animations;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGame.Components.Base;
using TheGame.Services.Camera;

namespace TheGame.Components.Tree1
{
    public partial class Tree1Component : BaseComponent<Tree1Instance>
    {
        // Properties
        public TimeSpan LastBoardPositionUpdateDelay { get; set; }
        public IAnimation TreeAnimation { get; set; }

        // Constructor
        public Tree1Component(TheGame game, IAnimationFactory animationFactory)
            : base(game, animationFactory)
        {
            Game = game;
            AnimationFactory = animationFactory;
        }

        // Methods
        public override void Initialize()
        {
            Id = new Guid("407ad527-7b7d-4a5c-bb9b-919752fd223d");
            Name = "Tree1";
            TextureFileName = "Tree1_51x64";
            TextureSize = new Vector2(56, 64);
            DrawingPositionType = PositionType.World;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            LoadAnimationTexture();
            TreeAnimation = Animations.AddAnimation("tree_animation", TextureSize, TimeSpan.Zero, false);

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            UpdateInstances(gameTime);
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            DrawInstances(gameTime);
            base.Draw(gameTime);
        }
    }
}
