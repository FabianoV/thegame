﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using TheGame.Components.Base;

namespace TheGame.Components.Tree1
{
    public class Tree1Instance : BaseInstance
    {
        // Properties

        // Constructor
        public Tree1Instance(TheGame game) 
            : base(game)
        {
            Position = new Vector2() { X = 100, Y = 200, };
            Scale = 1.0f;
        }

        // Methods
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
