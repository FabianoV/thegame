﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Animations;
using Microsoft.Xna.Framework;
using TheGame.Components.Base;
using TheGame.Services;

namespace TheGame.Components.CollisionShadow
{
    public class CollisionShadowInstance : BaseInstance
    {
        // Properties
        public IBoxCollisionInstance InstanceOfShadow { get; set; } // Should use for read only

        // Constructor
        public CollisionShadowInstance(TheGame game) 
            : base(game)
        {

        }

        // Methods
        public override void Update(GameTime gameTime)
        {
            Rectangle rect = InstanceOfShadow.CollisionBoxes.First();
            Position = new Vector2(rect.X, rect.Y);
            DrawingRectangle = new Vector2(rect.Width, rect.Height);
            Mask = InstanceOfShadow.CollisionBoxColor;

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            foreach (Rectangle box in InstanceOfShadow.CollisionBoxes)
            {
                Game.SpriteBatch.Draw(Animations, box, Mask);
            }
        }
    }
}
