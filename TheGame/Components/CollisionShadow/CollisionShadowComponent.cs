﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Animations;
using Microsoft.Xna.Framework;
using TheGame.Components.Base;
using TheGame.Services;

namespace TheGame.Components.CollisionShadow
{
    public class CollisionShadowComponent : BaseComponent<CollisionShadowInstance>
    {
        // Properties
        public IAnimation DefaultAnimation { get; set; }

        // Constructor
        public CollisionShadowComponent(TheGame game, IAnimationFactory animationFactory) 
            : base(game, animationFactory)
        {

        }

        // Methods
        public override void Initialize()
        {
            Id = new Guid("727b1df7-f27c-4b9f-aea6-6e0b4ff8ac2c");
            Name = "CollisionShadow";
            TextureFileName = "collisionshadow_50x50";
            TextureSize = new Vector2(50, 50);
            DrawOrder = (int)DrawingOrder.Particles;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            LoadAnimationTexture();
            DefaultAnimation = Animations.AddAnimation("Default", TextureSize, TimeSpan.Zero, false);
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            UpdateInstances(gameTime);
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            DrawInstances(gameTime);
            base.Draw(gameTime);
        }
    }
}
