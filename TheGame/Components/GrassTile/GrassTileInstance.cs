﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Animations;
using Microsoft.Xna.Framework;
using TheGame.Components.Base;

namespace TheGame.Components.GrassTile
{
    public class GrassTileInstance : BaseInstance
    {
        // Properties


        // Constuctor
        public GrassTileInstance(TheGame game) 
            : base(game)
        {
            Position = Vector2.Zero;
        }

        // Methods
        public override void Update(GameTime gameTime)
        {

        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
