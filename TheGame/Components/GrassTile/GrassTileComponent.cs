﻿using Animations;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGame.Components.Base;
using TheGame.Services;
using TheGame.Services.Camera;

namespace TheGame.Components.GrassTile
{
    public partial class GrassTileComponent : BaseComponent<GrassTileInstance>
    {
        // Properties
        public IAnimation GrassTileAnimation { get; set; }

        // Constructor
        public GrassTileComponent(TheGame game, IAnimationFactory animationFactory)
            : base(game, animationFactory)
        {
            Game = game;
            AnimationFactory = animationFactory;
        }

        // Methods
        public override void Initialize()
        {
            Id = new Guid("f6c95ec1-2740-4f6b-bebf-bf23da7a35b5");
            Name = "GrassTile";
            TextureFileName = "grasstile_16x16";
            TextureSize = new Vector2(16, 16);
            DrawOrder = (int)DrawingOrder.BackgoundTiles;
            DrawingPositionType = PositionType.World;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            LoadAnimationTexture();
            GrassTileAnimation = Animations.AddAnimation("Grass Tile Animation", TextureSize, TimeSpan.FromMilliseconds(200), isLooped: true);

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            DrawInstances(gameTime);
            base.Draw(gameTime);
        }
    }
}
