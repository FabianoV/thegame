﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Animations;
using Microsoft.Xna.Framework;
using TheGame.Components.Base;
using TheGame.Services;
using TheGame.Services.Camera;

namespace TheGame.Components.GreenBackground
{
    public class GreenBackgroundComponent : BaseComponent<GreenBackgroundInstance>
    {
        // Properties
        public IAnimation BackgroundAnimation { get; set; }

        // Constructor
        public GreenBackgroundComponent(TheGame game, IAnimationFactory animationFactory) 
            : base(game, animationFactory)
        {

        }

        // Methods
        public override void Initialize()
        {
            Id = new Guid("47777706-35ca-4693-b39a-9e90a7890e5d");
            Name = "GreenBackground";
            TextureFileName = "backgroundgreen_1x1";
            TextureSize = new Vector2(1, 1);
            DrawOrder = (int)DrawingOrder.BackgroundObjects;
            DrawingPositionType = PositionType.World;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            LoadAnimationTexture();
            BackgroundAnimation = Animations.AddAnimation("background_green_animation", TextureSize, TimeSpan.Zero, false);

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            DrawInstances(gameTime);
            base.Draw(gameTime);
        }
    }
}
