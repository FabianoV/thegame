﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Animations;
using Microsoft.Xna.Framework;
using TheGame.Components.Base;

namespace TheGame.Components.GreenBackground
{
    public class GreenBackgroundInstance : BaseInstance
    {
        // Constructor
        public GreenBackgroundInstance(TheGame game) 
            : base(game)
        {

        }

        // Methods
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
