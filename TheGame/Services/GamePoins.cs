﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.Services
{
    public class GamePoint
    {
        // Properties
        public float X { get; set; }
        public float Y { get; set; }

        // Methods
        public Vector2 ToVector2()
        {
            return new Vector2(X, Y);
        }

        public void FromVector2(Vector2 v)
        {
            X = v.X;
            Y = v.Y;
        }
    }
}
