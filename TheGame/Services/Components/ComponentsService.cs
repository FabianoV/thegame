﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TheGame.Components.Base;

namespace TheGame.Services.Components
{
    /// <summary>
    /// Class responsible for create and manage game components.
    /// </summary>
    public class ComponentsService : IComponentsService
    {
        // Properties
        public TheGame Game { get; set; }
        public List<IGameComponent> Components { get; set; }

        // Constructor
        public ComponentsService(TheGame game)
        {
            Game = game;
            Components = new List<IGameComponent>();
        }

        // Methods
        public void RegisterAllComponentsFromAssembly()
        {
            List<Type> componentsTypes = Assembly.GetAssembly(this.GetType())
                .GetTypes()
                .Where(type => type.GetInterfaces().Contains(typeof(IGameComponent)) && type != typeof(BaseComponent<>))
                .ToList();

            foreach (Type componentType in componentsTypes)
            {
                IGameComponent componentInstance = CreateComponent(componentType);
                Components.Add(componentInstance);
                Game.Components.Add(componentInstance);
            }
        }

        private IGameComponent CreateComponent(Type type)
        {
            var component = AutoFacBootstrap.Get(type) as IGameComponent;
            return component;
        }

        public IGameComponent Get(Guid componentId)
        {
            return Components.FirstOrDefault(c => (c as BaseComponent<BaseInstance>).Id == componentId);
        }

        public IGameComponent Get(string componentName)
        {
            return Components.FirstOrDefault(c => (c as BaseComponent<BaseInstance>).Name == componentName);
        }

        public IGameComponent Get(Type componentType)
        {
            return Components.FirstOrDefault(c => c.GetType() == componentType);
        }

        public ComponentType Get<ComponentType>()
        {
            var component = Components.FirstOrDefault(c => c is ComponentType);
            return (ComponentType)component;
        }
    }
}
