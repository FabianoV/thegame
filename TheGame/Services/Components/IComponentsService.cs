﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.Services.Components
{
    /// <summary>
    /// Class responsible for create and manage game components.
    /// </summary>
    public interface IComponentsService
    {

        IGameComponent Get(Guid componentId);
        IGameComponent Get(string componentName);
        IGameComponent Get(Type componentType);
        ComponentType Get<ComponentType>();


        void RegisterAllComponentsFromAssembly();
    }
}
