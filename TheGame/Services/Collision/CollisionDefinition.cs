﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGame.Components.Base;
using TheGame.Components.Cursor;
using TheGame.Components.House1;

namespace TheGame.Services.Collision
{
    public class CollisionDefinition : ICollisionDefinition
    {
        // Services
        public IBoxCollisionStoreService CollisionStore { get; set; }

        // Constructor
        public CollisionDefinition()
        {
            CollisionStore = AutoFacBootstrap.Get<IBoxCollisionStoreService>();
        }

        // Methods
        public void DefineCollisions()
        {
            // Cursor instance - house1 instances
            //var cursorInstance = GetComponent<CursorComponent>().Instances.First();
            //var house1Instances = GetComponent<House1Component>().Instances;
            //foreach (var houseInstance in house1Instances)
            //{
            //    CreateRule(cursorInstance, houseInstance);
            //}
        }

        private void CreateRule(BaseInstance i1, BaseInstance i2, string name = null)
        {
            if (string.IsNullOrEmpty(name))
            {
                name = $"Collision Rule [{i1.Name} - {i2.Name}]";
            }

            BoxCollisionRule rule = new BoxCollisionRule()
            {
                Name = name,
                First = i1,
                Second = i2,
            };

            CollisionStore.AddRule(rule);
        }

        private ComponentType GetComponent<ComponentType>()
        {
            return AutoFacBootstrap.Get<ComponentType>();
        }
    }
}
