﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.Services.Collision
{
    /// <summary>
    /// Represents a definition about collision beetwen two objects (First and Second).
    /// </summary>
    public class BoxCollisionRule
    {
        public string Name { get; set; }
        public bool Active { get; set; }
        public IBoxCollisionInstance First { get; set; }
        public IBoxCollisionInstance Second { get; set; }
    }
}
