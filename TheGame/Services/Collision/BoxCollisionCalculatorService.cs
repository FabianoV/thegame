﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.Services.Collision
{
    /// <summary>
    /// Service for managing collision shadows.
    /// </summary>
    public class BoxCollisionCalculatorService : IBoxCollisionCalculatorService
    {
        // Constructor
        public BoxCollisionCalculatorService()
        {
        }

        // Methods
        public bool CheckCollision(BoxCollisionRule rule)
        {
            foreach (Rectangle box1 in rule.First.CollisionBoxes)
            {
                foreach (Rectangle box2 in rule.Second.CollisionBoxes)
                {
                    if (CheckCollision(box1, box2) == true)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool CheckCollision(Rectangle rect1, Rectangle rect2)
        {
            if (rect1.Contains(rect2.X, rect2.Y) || 
                rect1.Contains(rect2.X + rect2.Width, rect2.Y) ||
                rect1.Contains(rect2.X, rect2.Y + rect2.Height) ||
                rect1.Contains(rect2.X + rect2.Width, rect2.Y + rect2.Height))
            {
                return true;
            }

            return false;
        }
    }
}
