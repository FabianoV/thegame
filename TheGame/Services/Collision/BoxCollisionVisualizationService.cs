﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGame.Components.Base;
using TheGame.Components.CollisionShadow;

namespace TheGame.Services.Collision
{
    public class BoxCollisionVisualizationService : IBoxCollisionVisualisationService
    {
        // Services
        public CollisionShadowComponent CollisionShadow { get; set; }

        // Constructor
        public BoxCollisionVisualizationService(CollisionShadowComponent collisionShadow)
        {
            CollisionShadow = collisionShadow;
        }

        // Methods
        public void CreateShadowFor(IBoxCollisionInstance instance)
        {
            var shadowInstance = AutoFacBootstrap.Get<CollisionShadowInstance>();
            shadowInstance.InstanceOfShadow = instance;
            CollisionShadow.RegisterInstance(shadowInstance);
        }

        public bool CheckExistsShadowFor(IBoxCollisionInstance instance)
        {
            foreach (var registeredInsatnce in CollisionShadow.Instances)
            {
                if (registeredInsatnce.Equals(instance))
                {
                    return true;
                }
            }

            return false;
        }

        public CollisionShadowInstance GetShadowInstanceFormInstance(IBoxCollisionInstance instance)
        {
            return CollisionShadow.Instances.First(i => i.InstanceOfShadow.Equals(instance));
        }

        public void RemoveShadowFor(IBoxCollisionInstance instance)
        {
            var shadowInsatnce = GetShadowInstanceFormInstance(instance);
            CollisionShadow.RemoveInstance(shadowInsatnce);
        }
    }
}
