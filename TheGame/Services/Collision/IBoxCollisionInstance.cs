﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.Services
{
    /// <summary>
    /// Implement this interface provides usage of box collisions mechanism.
    /// Define collision boxes and shadow color in visualization.
    /// </summary>
    public interface IBoxCollisionInstance
    {
        bool CollisionEnabled { get; set; }
        List<Rectangle> CollisionBoxes { get; set; }
        Color CollisionBoxColor { get; set; }
    }
}
