﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGame.Services.Collision;

namespace TheGame.Services
{
    /// <summary>
    /// Class that contains and handle all collision rules.
    /// </summary>
    public class BoxCollisionStoreService : IBoxCollisionStoreService
    {
        // Properties
        public List<BoxCollisionRule> Rules { get; set; }

        // Constructor
        public BoxCollisionStoreService()
        {
            Rules = new List<BoxCollisionRule>();
        }

        // Methods
        public BoxCollisionRule AddRule(string name, IBoxCollisionInstance first, IBoxCollisionInstance second, bool active=true)
        {
            var rule = new BoxCollisionRule()
            {
                Name = name,
                First = first,
                Second = second,
                Active = active,
            };
            Rules.Add(rule);
            return rule;
        }

        public BoxCollisionRule AddRule(BoxCollisionRule rule)
        {
            Rules.Add(rule);

            if (string.IsNullOrEmpty(rule.Name))
            {
                throw new ArgumentException("Rule have to have a name.");
            }

            return rule;
        }

        public List<BoxCollisionRule> GetRuleByObjectInstance(IBoxCollisionInstance instance)
        {
            return Rules.Where(r => r.First.Equals(instance) || r.Second.Equals(instance)).ToList();
        }
    }
}
