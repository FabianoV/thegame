﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.Services.Collision
{
    /// <summary>
    /// Collection with all collision rules in game.
    /// </summary>
    public interface IBoxCollisionStoreService
    {
        BoxCollisionRule AddRule(BoxCollisionRule rule);
        BoxCollisionRule AddRule(string name, IBoxCollisionInstance first, IBoxCollisionInstance second, bool active = true);
    }
}
