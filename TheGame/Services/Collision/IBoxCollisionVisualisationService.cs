﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGame.Components.CollisionShadow;

namespace TheGame.Services.Collision
{
    public interface IBoxCollisionVisualisationService
    {
        void CreateShadowFor(IBoxCollisionInstance instance);
        bool CheckExistsShadowFor(IBoxCollisionInstance instance);
        CollisionShadowInstance GetShadowInstanceFormInstance(IBoxCollisionInstance instance);
        void RemoveShadowFor(IBoxCollisionInstance instance);
    }
}
