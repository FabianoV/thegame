﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGame.Models.PixiJSReader;

namespace TheGame.Services.Tools
{
    public class PixiJSReader : IPixiJSReader
    {
        public PixiJSJson Read(string json)
        {
            var result = new PixiJSJson
            {
                Frames = new List<PixiJSFrame>(),
            };

            dynamic data = JsonConvert.DeserializeObject(json);

            foreach (var generalProperty in data)
            {
                var gProp = generalProperty.Value;

                if (generalProperty.Name == "frames")
                {
                    foreach (var frameProperty in generalProperty.Value)
                    {
                        var prop = frameProperty.Value;

                        PixiJSFrame frame = new PixiJSFrame()
                        {
                            Name = frameProperty.Name.Replace(".png", ""),
                            Frame = new PixiJSRectangle()
                            {
                                W = prop.frame.w,
                                H = prop.frame.h,
                                X = prop.frame.x,
                                Y = prop.frame.y,
                            },
                            Rotated = prop.rotated,
                            Trimmed = prop.trimmed,
                            SourceSize = new PixiJSSize()
                            {
                                W = prop.sourceSize.w,
                                H = prop.sourceSize.h,
                            },
                            SpriteSourceSize = new PixiJSRectangle()
                            {
                                X = prop.spriteSourceSize.x,
                                Y = prop.spriteSourceSize.y,
                                W = prop.spriteSourceSize.w,
                                H = prop.spriteSourceSize.h,
                            }
                        };

                        result.Frames.Add(frame);
                    }
                }

                if (generalProperty.Name == "meta")
                {
                    result.Meta = new PixiJSMeta()
                    {
                        App = gProp.app,
                        Format = gProp.format,
                        Image = gProp.image,
                        Version = gProp.version,
                        Size = new PixiJSSize()
                        {
                            W = gProp.size.w,
                            H = gProp.size.h,
                        },
                    };
                }
            }

            return result;
        }

        public PixiJSJson ReadFromFile(string path)
        {
            var fileJson = File.ReadAllText("./Content/Images/Mario/Mario/Red Big/Mario Red Big.json");
            return Read(fileJson);
        }
    }
}
