﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGame.Models.PixiJSReader;

namespace TheGame.Services.Tools
{
    public interface IPixiJSReader
    {
        PixiJSJson Read(string json);
        PixiJSJson ReadFromFile(string path);
    }
}
