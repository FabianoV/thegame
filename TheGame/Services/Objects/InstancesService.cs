﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TheGame.Components.Base;
using TheGame.Components.Cursor;
using TheGame.Components.GrassTile;
using TheGame.Components.House1;
using TheGame.Components.Log1Component;
using TheGame.Components.Mario;
using TheGame.Components.Player;
using TheGame.Components.Tree1;
using TheGame.Services.Components;

namespace TheGame.Services.Objects
{
    public class InstancesService : IInstancesService
    {
        // Services
        public TheGame Game { get; set; }
        public List<BaseInstance> Instances { get; set; }
        public IComponentsService ComponentsService { get; set; }

        // Constructor
        public InstancesService(TheGame game, IComponentsService componentsService)
        {
            Game = game;
            ComponentsService = componentsService;
            Instances = new List<BaseInstance>();
        }

        // Methods
        public void RegisterGameObjects()
        {
            // Get Components
            var marioComponent = ComponentsService.Get<MarioComponent>();
            //var grassTileComponent = ComponentsService.Get<GrassTileComponent>();
            var cursorComponent = ComponentsService.Get<CursorComponent>();
            //var house1Component = ComponentsService.Get<House1Component>();
            //var log1Component = ComponentsService.Get<Log1Component>();
            //var playerComponent = ComponentsService.Get<PlayerComponent>();
            //var tree1Component = ComponentsService.Get<Tree1Component>();

            // Create and register Instances
            var marioInstance = CreateInstance(marioComponent, "Mario");
            marioInstance.Scale = 4;

            var cursorInstance = CreateInstance(cursorComponent, "Cursor");

            //for (int x = 0; x < 40; x++)
            //{
            //    for (int y = 0; y < 40; y++)
            //    {
            //        var grassTileInstance = CreateInstance(grassTileComponent, $"Grass Tile [{x}, {y}]");
            //        grassTileInstance.Position = new Vector2(x * (grassTileComponent.TextureSize.X * 2), y * (grassTileComponent.TextureSize.Y * 2));
            //        grassTileInstance.Scale = 3f;
            //        grassTileInstance.RecalculateDrawingRectangleByImageScale();
            //    }
            //}

            //var house1Instance1 = CreateInstance(house1Component, "House1 insatnce 1");
            //house1Instance1.Position = new Vector2(100, 100);
            //var house1Instance2 = CreateInstance(house1Component, "House1 insatnce 2");
            //house1Instance2.Position = new Vector2(100, 200);

            //var log1Instance = CreateInstance(log1Component, "Log1 instance 1");
            //log1Instance.Position = new Vector2(60, 150);

            //var playerInstance = CreateInstance(playerComponent, "Player instance");
            //playerInstance.Position = new Vector2(200, 200);
            //playerInstance.Scale = 2.0f;
            //playerInstance.RecalculateDrawingRectangleByImageScale();

            //var tree1Instance = CreateInstance(tree1Component, "Tree1 instance 1");
            //tree1Instance.Position = new Vector2(50, 50);
            //tree1Instance.Scale = 1.6f;
            //tree1Instance.RecalculateDrawingRectangleByImageScale();
        }

        private InstanceType CreateInstance<InstanceType>(BaseComponent<InstanceType> component, string instanceName) where InstanceType : BaseInstance
        {
            InstanceType instance = AutoFacBootstrap.Get<InstanceType>();
            instance.Name = instanceName;
            component.RegisterInstance(instance);
            Instances.Add(instance);
            return instance;
        }
    }
}
