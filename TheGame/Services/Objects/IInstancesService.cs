﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.Services.Objects
{
    public interface IInstancesService
    {
        void RegisterGameObjects();
    }
}
