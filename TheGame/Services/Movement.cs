﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.Services
{
    /// <summary>
    /// Contains logic that calculates movement.
    /// </summary>
    public class MovementService
    {
        // Properties
        public float Speed { get; set; } // Pixels per second

        // Method
        public void Update(GameTime gameTime, GameArea positionState, KeyboardState keboardState)
        {
            if (keboardState.IsKeyDown(Keys.A))
            {
                positionState.X = (float)(positionState.X - (Speed * gameTime.ElapsedGameTime.TotalSeconds));
            }
            else if (keboardState.IsKeyDown(Keys.D))
            {
                positionState.X = (float)(positionState.X + (Speed * gameTime.ElapsedGameTime.TotalSeconds));
            }
            else if (keboardState.IsKeyDown(Keys.W))
            {
                positionState.Y = (float)(positionState.Y - (Speed * gameTime.ElapsedGameTime.TotalSeconds));
            }
            else if (keboardState.IsKeyDown(Keys.S))
            {
                positionState.Y = (float)(positionState.Y + (Speed * gameTime.ElapsedGameTime.TotalSeconds));
            }
        }
    }
}
