﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.Services
{
    public class GameSize
    {
        // Properties
        public int Width { get; set; }
        public int Height { get; set; }

        // Methods
        public Vector2 ToVector2()
        {
            return new Vector2(Width, Height);
        }

        public void FromVector2(Vector2 v)
        {
            Width = (int)v.X;
            Height = (int)v.Y;
        }
    }
}
