﻿using Animations;
using Comora;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.IO;
using TheGame.Services.Collision;
using TheGame.Services.Components;
using TheGame.Services.Objects;
using TheGame.Services.Tools;

namespace TheGame
{
    public class TheGame : Game
    {
        // Services
        public GraphicsDeviceManager Graphics { get; set; }
        public SpriteBatch SpriteBatch { get; set; }
        public IComponentsService ComponentsService { get; set; }
        public IInstancesService InstancesDefinition { get; set; }
        public ICollisionDefinition CollisionDefinition { get; set; }
        public IBoxCollisionStoreService CollisionStore { get; set; }
        public IBoxCollisionVisualisationService CollisionVisualization { get; set; }

        public Camera Camera { get; set; }

        // Data
        public int ScreenWidth { get; set; } = 1000;
        public int ScreenHeight { get; set; } = 896;

        // Constructor
        public TheGame()
        {
            Graphics = new GraphicsDeviceManager(this);
            Graphics.IsFullScreen = false;
            Graphics.PreferredBackBufferWidth = ScreenWidth;
            Graphics.PreferredBackBufferHeight = ScreenHeight;
            Graphics.ApplyChanges();
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            // Services
            InstancesDefinition = AutoFacBootstrap.Get<IInstancesService>();
            CollisionDefinition = AutoFacBootstrap.Get<ICollisionDefinition>();
            CollisionStore = AutoFacBootstrap.Get<IBoxCollisionStoreService>();
            CollisionVisualization = AutoFacBootstrap.Get<IBoxCollisionVisualisationService>();
            ComponentsService = AutoFacBootstrap.Get<IComponentsService>();
            Camera = new Camera(GraphicsDevice);

            ComponentsService.RegisterAllComponentsFromAssembly();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);
            Camera.LoadContent();

            base.LoadContent();

            InstancesDefinition.RegisterGameObjects();
            CollisionDefinition.DefineCollisions();
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            // var oldViewport = this.GraphicsDevice.Viewport;
            // this.GraphicsDevice.Viewport = new Viewport(oldViewport.X - 1, oldViewport.Y, ScreenWidth, ScreenHeight);
            Camera.Update(gameTime);

            Camera.Debug.IsVisible = Keyboard.GetState().IsKeyDown(Keys.F1);

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            SpriteBatch.Draw(Camera.Debug);
            base.Draw(gameTime);
        }
    }
}
