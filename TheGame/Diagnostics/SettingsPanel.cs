﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheGame.Diagnostics
{
    public partial class SettingsPanel : Form
    {
        // Services
        public TheGame Game { get; set; }

        // Constructor
        public SettingsPanel()
        {
            InitializeComponent();
        }

        // Methods
        public void Initialize(TheGame game)
        {
            Game = game;
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            listBoxComponents.Items.Clear();
            foreach (IGameComponent item in Game.Components)
            {
                listBoxComponents.Items.Add(item.ToString());
            }
            labelComponentsCount.Text = "Components count: " + Game.Components.Count;
        }
    }
}
