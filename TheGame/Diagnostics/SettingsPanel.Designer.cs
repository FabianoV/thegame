﻿namespace TheGame.Diagnostics
{
    partial class SettingsPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.listBoxComponents = new System.Windows.Forms.ListBox();
            this.labelComponentsCount = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(759, 12);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdate.TabIndex = 1;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // listBoxComponents
            // 
            this.listBoxComponents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxComponents.FormattingEnabled = true;
            this.listBoxComponents.Location = new System.Drawing.Point(12, 12);
            this.listBoxComponents.Name = "listBoxComponents";
            this.listBoxComponents.Size = new System.Drawing.Size(741, 407);
            this.listBoxComponents.TabIndex = 2;
            // 
            // labelComponentsCount
            // 
            this.labelComponentsCount.AutoSize = true;
            this.labelComponentsCount.Location = new System.Drawing.Point(13, 426);
            this.labelComponentsCount.Name = "labelComponentsCount";
            this.labelComponentsCount.Size = new System.Drawing.Size(100, 13);
            this.labelComponentsCount.TabIndex = 3;
            this.labelComponentsCount.Text = "Components Count:";
            // 
            // SettingsPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 443);
            this.Controls.Add(this.labelComponentsCount);
            this.Controls.Add(this.listBoxComponents);
            this.Controls.Add(this.buttonUpdate);
            this.Name = "SettingsPanel";
            this.Text = "SettingsPanel";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.ListBox listBoxComponents;
        private System.Windows.Forms.Label labelComponentsCount;
    }
}