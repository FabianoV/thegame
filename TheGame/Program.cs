﻿using System;
using SPanel = TheGame.Diagnostics;

namespace TheGame
{
#if WINDOWS || LINUX
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            AutoFacBootstrap.Initialize();
            using (TheGame game = AutoFacBootstrap.Get<TheGame>())
            {
                game.Run();
            }
        }
    }
#endif
}
